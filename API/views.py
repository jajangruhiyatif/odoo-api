from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.core.cache import cache
from django.db import connection

import array as arr
import json
import pdb

from API.constant import Token

def validation_api(key):
	if 'a53c6d8a114ebf02d0fb05782534c738bb8f1c8845' == key:
		return True
	else:
		return False

def refresh(request):
	if request.method == 'GET':
		cache.delete('cache_product')
		cache.delete('cache_reminder')
		cache.delete('cache_transaction')

		text = 'Data has been refreshed !'

	return HttpResponse(text, content_type='text/application')

class UserLogin():
	@csrf_exempt
	def getPersonNumber(request):
		if request.method == 'POST':
			payload = json.loads(request.body)
			number = payload['telp']
			if len(number) >= 10:
				try:
					num = UserLogin.validateInputNumber(number)
					# num = number
					with connection.cursor() as cursor:
						cursor.execute("""
										SELECT name 
												,phone 
												,customer 
												,email
												,apps_password
												,id
										FROM res_partner 
										WHERE customer = 't' AND 
											phone LIKE %s
										LIMIT 1
										"""
									,['%'+num+'%'])
						data = cursor.fetchall()
						response = []
						for row in data:
							response = json.dumps({
												'name': row[0],
												'mobile': UserLogin.validateOutputNumber(row[1]),
												'customer': row[2],
												'email': row[3],
												'password': row[4],
												'user_id': row[5],
												'status': True
											})
						if(response==[]) :
							response = json.dumps({'Warn': 'Data tidak ditemukan'})
				except Exception as e:
					response = json.dumps({'Failed': 'Terjadi kesalahan saat mencari data anda'})
			else:
				response = json.dumps({'Warn': 'Check your number phone'})
		return HttpResponse(response, content_type='text/json')

	def validateInputNumber(number):
		if number[0] == '+':
			num = number.replace('+62', '')
		elif number[0] == '6':
			num = number.replace('62', '')
		elif number[0] == '0':
			x = "+"+number
			num = x.replace('+0', '')
		else:
			num = number
		return num

	def validateOutputNumber(number):
		if number[0] == '+':
			num = number.replace('+62', '0')
		elif number[0] == '6':
			num = number.replace('62', '0')
		elif number[0] == '0':
			x = "+"+number
			num = x.replace('+0', '0')
		else:
			num = number
		return num

class EstimatedServices():
	@csrf_exempt
	def getVehicle(request):
		param = request.GET.get('term')
		cache_key = 'cache_type'
		cache_time = 900
		data = cache.get(cache_key)
		try:
			with connection.cursor() as cursor:
				cursor.execute(""" select distinct name 
									from
										product_template_vehicle_model 
									where
										name like initcap('%"""+param+"""%')
									LIMIT 20
								""")
				result = cursor.fetchall()

				if result:
					arr = []
					i = 0
					for row in result:
						arr.append({'id':result[i][0], 'text':result[i][0]})
						i += 1
					response = json.dumps(arr)
				else:
					arr = []
					arr.append({'id':0, 'text':'Data tidak ditemukan'})
					response = json.dumps(arr)
			cache.set(cache_key, response, cache_time)
			data = cache.get(cache_key)
		except Exception as e:
			response = e
			data = cache.set(response)
		return HttpResponse(data, 'application/json')

	@csrf_exempt
	def getOilPrice(request):
		if request.method == 'POST':
			payload = json.loads(request.body)
			oil_type = payload['oil_type']
			vehicle  = payload['txt_vehicle']
			if validation_api(payload['key_token']):
				try:
					with connection.cursor() as cursor:
						cursor.execute("""
										select a.oil_type, i.kapasitas_oli_mesin * a.list_price, h.name, a.name, i.kapasitas_oli_mesin
										from
										product_template a left join product_product b
										on a.id = b.product_tmpl_id left join product_tire_type c
										on a.pb_tire_type = c.id left join product_tire_ring d
										on a.pb_tire_ring = d.id left join product_tire_size e
										on a.pb_tire_size = e.id left join product_tire_brand f
										on a.pb_tire_brand = f.id left join product_category g
										on a.categ_id = g.id left join product_template_vehicle_model h
										on a.id = h.product_template_id left join mobeng_buku_pintar i
										on h.models_ids_one2many = i.model_id
										where
										a.active = 't'
										and a.website_published = 't'
										and a.oil_type = %s
										and h.name LIKE %s
										""",
										[oil_type, '%'+vehicle+'%'])
						data = cursor.fetchone()
						arr = []
						if data:
							# for row in data:
							arr.append({
								'oil_type': data[0],
								'price': data[1],
								'vehicle': data[2],
								'product_nm': data[3],
								'capacity': data[4]
								})
							response = json.dumps(arr, default=str)
						else:
							arr.append({
								'oil_type': oil_type,
								'price': "0",
								'vehicle': "",
								'product_nm': "",
								'capacity': "0"
								})
							response = json.dumps(arr, default=str)
				except Exception as e:
					response = e
			else:
				response = json.dumps({'Warn': 'Access Denied'})
		return HttpResponse(response, 'application/json')

	@csrf_exempt
	def getTirePrice(request):
		if request.method == 'POST':
			payload = json.loads(request.body)
			tire_size = payload['ring_size']
			vehicle = payload['txt_vehicle']
			if validation_api(payload['key_token']):
				try:
					with connection.cursor() as cursor:
						cursor.execute("""
										SELECT a.name as size
												,b.list_price
												,b.vehiclemodellist
											FROM 
												product_tire_ring a
											JOIN 
												product_template b on a.id = b.pb_tire_ring
											JOIN
												pos_category c on b.pos_categ_id = c.id
											WHERE 
												c.id = 2 AND
												b.active = 't' AND 
												b.website_published = 't' AND
												a.name = %s AND
												b.vehiclemodellist LIKE initcap(%s)
										LIMIT 1
										""", 
										[tire_size, '%'+vehicle+'%'])
						data = cursor.fetchone()
						arr = []
						if data:
							# for row in data:
							arr.append({	
								'tire_size': data[0],
								'price': data[1],
								'vehicle': data[2]
								})
							response = json.dumps(arr, default=str)
						else:
							arr.append({	
									'tire_size': tire_size,
									'price': "0",
									'vehicle': ""
									})
							response = json.dumps(arr, default=str)
				except Exception as e:
					response = e
			else:
				response = json.dumps({'Warn': 'Access Denied'})
		return HttpResponse(response, 'application/json')

	@csrf_exempt
	def getAccuPrice(request):
		if request.method == 'POST':
			payload = json.loads(request.body)
			vehicle = payload['txt_vehicle']
			if validation_api(payload['key_token']):
				try:
					with connection.cursor() as cursor:
						cursor.execute("""
										SELECT  a.vehiclemodellist
												,a.list_price 
										FROM product_template a
										JOIN pos_category b on a.pos_categ_id = b.id
										WHERE 
											b.id = 12 AND 
											a.vehiclemodellist LIKE initcap(%s) AND 
											a.active = 't' AND 
											a.website_published = 't'
										""", ['%'+vehicle+'%'])
						data = cursor.fetchone()
						arr = []
						if data:
							arr.append({	
								'vehicle': data[0],
								'price': data[1]
								})
							response = json.dumps(arr, default=str)
						else:
							arr.append({	
								'vehicle': "",
								'price': "0",
								})
							response = json.dumps(arr, default=str)
				except Exception as e:
					response = e
			else:
				response = json.dumps({'Warn': 'Access Denied'})
		return HttpResponse(response, content_type='application/json')

class UserNotice():
	@csrf_exempt
	def reminder(request):
		if request.method == 'POST':
			payload = json.loads(request.body)
			cache_key = 'cache_reminder'
			cache_time = 900
			data = cache.get(cache_key)
			if validation_api(payload['key_token']):
				try:
					# if not data:
					with connection.cursor() as cursor:
						cursor.execute(""" select
												a.active,
												a.website_published,
												a.name,
												a.list_price as public_price,
												a.vehiclemodellist,
												d.name as Ring,
												e.name as Size,
												f.name as Brand,
												c.name as Type,
												a.oil_type,
												b.default_code,
												g.reminder,
												TO_CHAR(g.write_date:: DATE, 'dd/mm/yyyy'),
												g.write_date + ('6 month'::interval) as dt_reminder
											from
												product_template a
												left join product_product b on a.id = b.product_tmpl_id
												left join product_tire_type c on a.pb_tire_type = c.id
												left join product_tire_ring d on a.pb_tire_ring = d.id
												left join product_tire_size e on a.pb_tire_size = e.id
												left join product_tire_brand f on a.pb_tire_brand = f.id
												left join product_category g on a.categ_id = g.id
											where
												a.active = 't'
												and a.website_published = 't'""")
						row = cursor.fetchone()
						response = row
						# pdb.set_trace()
				except Exception as e:
					response = json.dumps({'err': 'Something wrong'})
					data = cache.set(cache_key, response, cache_time)
			else:
				result = json.dumps({'ERR': 'Access Denied'})
		return JsonResponse(data, safe=False)

class Products():
	@csrf_exempt
	def type(request):
		if request.method == 'POST':
			payload = json.loads(request.body)
			types = payload['type']
			if validation_api(payload['key_token']):
				try:
					cache_key = 'cache_product_type'
					cache_time = 900
					# data = cache.get(cache_key)
					arr = []
					i = 0
					# if not data:
					with connection.cursor() as cursor:
						cursor.execute(""" select pc.name from pos_category as pc where is_group=%s""",[types])
						response = cursor.fetchall()
						for row in response:
							arrDet = []
							j = 0
							with connection.cursor() as cursorx:
								cursorx.execute(""" SELECT
									ptb.name FROM product_template as pt
									LEFT JOIN product_tire_brand as ptb on(pt.pb_tire_brand=ptb.id)
									LEFT JOIN pos_category as pc on(pt.pos_categ_id=pc.id)
									WHERE pc.name=%s AND ptb.name IS NOT NULL AND pt.active=true GROUP BY ptb.name """,[response[i][0]])
								detail = cursorx.fetchall()
								for row in detail:
									arrDet.append({'name':detail[j][0]})
									j += 1
							arr.append({'name':response[i][0], 'brand':arrDet})
							i += 1
					cache.set(cache_key, arr, cache_time)
					data = cache.get(cache_key)
				except Exception as e:
					data = cache.get(cache_key)
			else:
				result = json.dumps({'ERR': 'Access Denied'})
		return JsonResponse(data, safe=False)

	@csrf_exempt
	def brand(request):
		if request.method == 'POST':
			payload = json.loads(request.body)
			types = payload['type']
			if validation_api(payload['key_token']):
				try:
					cache_key = 'cache_product_brand'
					cache_time = 900
					# data = cache.get(cache_key)
					arr = []
					i = 0
					# if not data:
					with connection.cursor() as cursor:
						cursor.execute(""" SELECT
							ptb.name FROM product_template as pt
							LEFT JOIN product_tire_brand as ptb on(pt.pb_tire_brand=ptb.id)
							LEFT JOIN pos_category as pc on(pt.pos_categ_id=pc.id)
							WHERE pc.name=%s AND ptb.name IS NOT NULL AND pt.active=true GROUP BY ptb.name """,[types])
						response = cursor.fetchall()
						for row in response:
							arr.append({'name':response[i][0]})
							i += 1
					cache.set(cache_key, arr, cache_time)
					data = cache.get(cache_key)
				except Exception as e:
					response = json.dumps({'err': 'Something wrongss'})
					data = cache.get(cache_key)
			else:
				result = json.dumps({'ERR': 'Access Denied'})
		return JsonResponse(data, safe=False)

	@csrf_exempt
	def list(request):
		if request.method == 'POST':
			payload = json.loads(request.body)
			types = payload['type']
			brand = payload['brand']
			if validation_api(payload['key_token']):
				try:
					cache_key = 'cache_product_list'
					cache_time = 900
					# data = cache.get(cache_key)
					arr = []
					i = 0
					# if not data:
					with connection.cursor() as cursor:
						exQuery = ''
						if brand != "all":
							exQuery = """AND ptb.name='"""+brand+"""' """

						cursor.execute(""" SELECT pt.name, pt.list_price, pt.image1
							FROM product_template as pt
							LEFT JOIN product_tire_brand as ptb on(pt.pb_tire_brand=ptb.id)
							LEFT JOIN pos_category as pc on(pt.pos_categ_id=pc.id)
							WHERE pt.active=true AND pc.name='"""+types+"""' """+exQuery)
						response = cursor.fetchall()
						for row in response:
							arr.append({'name':response[i][0], 'price':response[i][1], 'img':response[i][2]})
							i += 1
					cache.set(cache_key, arr, cache_time)
					data = cache.get(cache_key)
				except Exception as e:
					response = json.dumps({'err': 'Something wrong'})
					data = cache.set(cache_key, response, cache_time)
			else:
				result = json.dumps({'ERR': 'Access Denied'})
		return JsonResponse(data, safe=False)

	@csrf_exempt
	def search(request):
		if request.method == 'POST':
			payload = json.loads(request.body)
			term = payload['term']
			if validation_api(payload['key_token']):
				try:
					cache_key = 'cache_product_search'
					cache_time = 900
					# data = cache.get(cache_key)
					arr = []
					i = 0
					# if not data:
					with connection.cursor() as cursor:
						cursor.execute(""" SELECT pt.name, pt.list_price, pt.image1
							FROM product_template as pt
							WHERE pt.active=true AND pt.name like initcap('%"""+term+"""%') """)
						response = cursor.fetchall()
						for row in response:
							arr.append({'name':response[i][0], 'price':response[i][1], 'img':response[i][2]})
							i += 1
					cache.set(cache_key, arr, cache_time)
					data = cache.get(cache_key)
				except Exception as e:
					response = json.dumps({'err': 'Something wrong'})
					data = cache.set(cache_key, response, cache_time)
			else:
				result = json.dumps({'ERR': 'Access Denied'})
		return JsonResponse(data, safe=False)

	@csrf_exempt
	def reminder(request):
		if request.method == 'POST':
			payload = json.loads(request.body)
			user_id = payload['user_id']
			if validation_api(payload['key_token']):
				try:
					cache_key = 'cache_product_reminder'
					cache_time = 900
					# data = cache.get(cache_key)
					arr = []
					# if not data:
					with connection.cursor() as cursor:
						cursor.execute(""" select
							a.partner_id as id,
							b.name as customer,
							b.phone,
							b.nopol,
							date(a.date_order),
							(a.date_order+interval '1 month' * replace(h.reminder, 'M', '')::int) as date_target,
							f.name_template as Product,
							e.price_unit,
							h.reminder as service_reminder,
							km_saat_ini,
							a.note,
							e.id
						from
							pos_order a
							left join res_partner b on a.partner_id = b.id
							left join account_invoice c on a.name = c.origin
							left join account_analytic_account d on c.analytic_account_id = d.id
							left join account_invoice_line e on c.id = e.invoice_id
							left join product_product f on e.product_id = f.id
							left join product_template g on f.product_tmpl_id = g.id
							left join product_category h on g.categ_id = h.id
						where
							c.amount_total > 0
							and a.approve_date is null
							and a.state = 'invoiced'
							and a.partner_id = %s
							and h.reminder<>'None'
							and now() >= (a.date_order+interval '1 month' * (replace(h.reminder, 'M', '')::int - 1))
							and now() <= ((a.date_order+interval '1 month' * replace(h.reminder, 'M', '')::int)+ interval '1 day' * 15)
						order by a.date_order desc """
						,[user_id])
						response = cursor.fetchall()

						i = 0
						for row in response:
							arr.append({
									'user_id':response[i][0],
									'name':response[i][1],
									'phone_number':response[i][2],
									'nopol':response[i][3],
									'trx_date':response[i][4].strftime("%d-%m-%Y") if response[i][4] else response[i][4],
									'target_date':response[i][5].strftime("%d-%m-%Y") if response[i][5] else response[i][5],
									'product':response[i][6],
									'price':response[i][7],
									'interval':response[i][8],
									'km':response[i][9],
									'note':response[i][10],
									'acc_invoice': response[i][11]
								})
							i += 1
					cache.set(cache_key, arr, cache_time)
					# data = cache.get(cache_key)
					data = arr
				except Exception as e:
					response = json.dumps({'err': 'Something wrong'})
					data = cache.get(cache_key)
			else:
				result = json.dumps({'ERR': 'Access Denied'})
		return JsonResponse(data, safe=False)

	@csrf_exempt
	def detail_reminder(request):
		if request.method == 'POST':
			cache_key = 'cache_detail_reminder'
			cache_time = 5
			arr = []

			payload = json.loads(request.body)
			user_id = payload['user_id']
			invoice = payload['invoice']
			if validation_api(payload['key_token']):
				try:
					with connection.cursor() as cursor:
						cursor.execute(""" select
							d.name as toko,
							b.name as customer,
							concat(b.street,b.city) as Alamat,
							b.phone,
							b.nopol,
							c.number as invoice,
							date(a.date_order),
							f.name_template as Product,
							e.price_unit,
							e.quantity,
							h.reminder as service_reminder,
							i.pemakaian_kendaraan,
							i.perkiraan_jarak,
							i.jumlah_muatan,
							i.trek_jalan,
							i.kondisi_lalin,
							i.bahan_bakar,
							i.kondisi_berkendara,
							i.km_jenis_oli_mesin1 as KM_Berikutnya,
							date(a.date_order+interval '1 month' * replace(h.reminder, 'M', '')::int) as next_date,
							k.name as created_by,
							g.oil_type,
							l.modelname as vehicle_model,
							km_saat_ini,
							i.tanggal_penggantian as back_date 
						from
							pos_order a
							left join res_partner b on a.partner_id = b.id
							left join account_invoice c on a.name = c.origin
							left join account_analytic_account d on c.analytic_account_id = d.id
							left join account_invoice_line e on c.id = e.invoice_id
							left join product_product f on e.product_id = f.id
							left join product_template g on f.product_tmpl_id = g.id
							left join product_category h on g.categ_id = h.id
							left join mobeng_pocs i on i.pos_ref = a.id
							left join res_users j on j.id = i.user_id
							left join res_partner k on k.id = j.partner_id
							left join fleet_vehicle_model l on b.model_id = l.id
						where
							a.partner_id = %s
							and e.id = %s
						order by a.date_order desc
						limit 1
						""",
						[user_id, invoice])
						response = cursor.fetchone()
						arr.append({
								'nama_toko': response[0],
								'pelanggan': response[1],
								'alamat': response[2],
								'phone': response[3],
								'nopol': response[4],
								'invoice': response[5],
								'date_order': response[6].strftime("%d-%m-%Y") if response[6] else response[6],
								'product': response[7],
								'price_unit': response[8],
								'qty': response[9],
								'service_reminder': response[10],
								'pemakaian_kendaraan': response[11],
								'perkiraan_jarak': response[12],
								'muatan': response[13],
								'trek': response[14],
								'lalulintas': response[15],
								'bahan_bakar': response[16],
								'kondisi_berkendara': response[17],
								'next_km': response[18],
								'next_date': response[19].strftime("%d-%m-%Y") if response[19] else response[19],
								'created_by': response[20],
								'oil_type': response[21],
								'km_saat_ini': response[23],
								'vehicle_model': response[22],
								'back_date': response[24].strftime("%d-%m-%Y") if response[24] else response[24]
							})
					# cache.set(cache_key, arr, cache_time)
					# data = cache.get(cache_key)
					# pdb.set_trace()
				except Exception as e:
					response = e
					# cache.set(cache_key, '01', cache_time)
					# data = cache.get(cache_key)
			else:
				response = json.dumps({'ERR': 'Access Denied'})
				# data = cache.get(cache_key)
		return JsonResponse(arr, safe=False)
		# return HttpResponse(data, content_type="application/json")

	@csrf_exempt
	def count_reminder(request):
		if request.method == 'POST':
			payload = json.loads(request.body)
			user_id = payload['user_id']
			if validation_api(payload['key_token']):
				try:
					with connection.cursor() as cursor:
						cursor.execute(""" 
							select
								count(*) as cnt
							from
								pos_order a
								left join res_partner b on a.partner_id = b.id
								left join account_invoice c on a.name = c.origin
								left join account_analytic_account d on c.analytic_account_id = d.id
								left join account_invoice_line e on c.id = e.invoice_id
								left join product_product f on e.product_id = f.id
								left join product_template g on f.product_tmpl_id = g.id
								left join product_category h on g.categ_id = h.id
							where
								c.amount_total > 0
								and a.approve_date is null
								and a.state = 'invoiced'
								and a.partner_id=%s
								and h.reminder<>'None'
								and (a.date_order+interval '1 month' * replace(h.reminder, 'M', '')::int) <= now()
							""", [user_id])
						data = cursor.fetchone()
						response = json.dumps({'count': data[0]})
				except Exception as e:
					response = e
		return HttpResponse(response, 'application/json')

	@csrf_exempt
	def brand_by_id(request):
		if request.method == 'POST':
			payload = json.loads(request.body)
			types = payload['type']
			if validation_api(payload['key_token']):
				try:
					cache_key = 'cache_product_brand_id'
					cache_time = 900
					arr = []
					i = 0
					with connection.cursor() as cursor:
						cursor.execute("""
							 SELECT
								ptb.name 
							FROM product_template as pt
								LEFT JOIN product_tire_brand as ptb on(pt.pb_tire_brand=ptb.id)
								LEFT JOIN pos_category as pc on(pt.pos_categ_id=pc.id)
							WHERE pc.name=initcap('"""+types+"""') AND ptb.name IS NOT NULL 
							GROUP BY ptb.name""")
						response = cursor.fetchall()
						for row in response:
							arr.append({'brand': response[i][0]})
							i += 1
					cache.set(cache_key, arr, cache_time)
					data = cache.get(cache_key)
				except Exception as e:
					response = json.dumps({'err': 'Something wrong'})
					data = cache.get(cache_key)
			else:
				data = json.dumps({'ERR': 'Access Denied'})
		return JsonResponse(data, safe=False)

class Transactions():
	@csrf_exempt
	def browse(request):
		if request.method == 'POST':
			payload = json.loads(request.body)
			user_id = payload['user_id']
			try:
				# pdb.set_trace()
				# cache_key = 'cache_get_transaction' + user_id
				# cache_time = 900
				with connection.cursor() as cursor:
					cursor.execute(""" 
									select
										c.number as invoice,
										b.name as customer,
										b.nopol,
										date(a.date_order),
										SUM(e.price_unit) as harga,
										l.modelname,
										km_saat_ini
									from
										pos_order a
										left join res_partner b	on a.partner_id = b.id
										left join account_invoice c on a.name = c.origin
										left join account_analytic_account d on c.analytic_account_id = d.id
										left join account_invoice_line e on c.id = e.invoice_id
										left join fleet_vehicle_model l on b.model_id = l.id
									where
										c.amount_total > 0
										and a.approve_date is null
										and a.state = 'invoiced'
										and b.id = %s
									group by 
										c.number,
										b.name,
										b.nopol,
										a.date_order,
										l.modelname,
										km_saat_ini
									order by a.date_order desc
									""", [user_id])
					data = cursor.fetchall()
					if data:
						arr = []
						i = 0
						for row in data:
							arr.append({
								'invoice': data[i][0],
								'customer': data[i][1],
								'nopol': data[i][2],
								'date': data[i][3].strftime("%d-%m-%Y") if data[i][3] else data[i][3],
								'harga': data[i][4],
								'vehicle_model': data[i][5],
								'km': data[i][6]
								})
							i += 1
				# cache.set(cache_key, arr, cache_time)
				response = arr
			except Exception as e:
				response = json.dumps({'status': 'false'})
		return JsonResponse(response, safe=False)

	@csrf_exempt
	def detail_transaction(request):
		if request.method == 'POST':
			try:
				payload = json.loads(request.body)
				invoice_no = payload['invoice_no']
				cache_key = 'cache_transaction'
				cache_time = 900
				arr = []
				arr_total = []

				with connection.cursor() as cursor:
					cursor.execute(""" 
									select
										f.name_template as Product,
										e.quantity,
										e.price_unit,
										e.discount,
										(e.price_unit * e.quantity) as subtotal
									from
										pos_order a
										left join res_partner b	on a.partner_id = b.id
										left join account_invoice c on a.name = c.origin
										left join account_analytic_account d on c.analytic_account_id = d.id
										left join account_invoice_line e on c.id = e.invoice_id
										left join product_product f on e.product_id = f.id
										left join product_template g on f.product_tmpl_id = g.id
										left join product_category h on g.categ_id = h.id
									where
										c.amount_total > 0
										and a.approve_date is null
										and a.state = 'invoiced'
										and c.number = '"""+invoice_no+"""'
									""")
					data = cursor.fetchall()

					if data:
						i = 0
						for row in data:
							arr.append({
								'item': data[i][0],
								'qty': data[i][1],
								'harga': data[i][2],
								'disc': data[i][3],
								'subtotal': data[i][4]
								})
							i += 1

				with connection.cursor() as sqlExec:
					sqlExec.execute(""" 
									select	
					 					c.number as invoice,
					 					date(a.date_order),
					 					b.name as customer,
					 					b.nopol,
					 					km_saat_ini,
					 					SUM(e.price_unit) as harga,
										l.modelname
					 				from
					 					pos_order a
					 					left join res_partner b	on a.partner_id = b.id
					 					left join account_invoice c on a.name = c.origin
					 					left join account_analytic_account d on c.analytic_account_id = d.id
					 					left join account_invoice_line e on c.id = e.invoice_id
					 					left join product_product f on e.product_id = f.id
					 					left join product_template g on f.product_tmpl_id = g.id
					 					left join product_category h on g.categ_id = h.id
										left join fleet_vehicle_model l on b.model_id = l.id
					 				where
					 					c.amount_total > 0
					 					and a.approve_date is null
					 					and a.state = 'invoiced'
					 					and c.number = '"""+invoice_no+"""'
					 				group by 
					 					c.number,
					 					b.name,
					 					b.nopol,
					 					a.date_order,
					 					b.id,
					 					km_saat_ini,
					 					l.modelname
									""")
					data_transaksi = sqlExec.fetchone()

					if data_transaksi:
						arr_total.append({
							'invoice_no': data_transaksi[0],
							'date': data_transaksi[1].strftime("%d-%m-%Y") if data_transaksi[1] else data_transaksi[1],
							'customer': data_transaksi[2],
							'nopol': data_transaksi[3],
							'km': data_transaksi[4],
							'total_harga': data_transaksi[5],
							'vehicle_model': data_transaksi[6],
							'detail_transaction': arr,
							})
					response = json.dumps(arr_total, default=str)
				cache.set(cache_key, response, cache_time)
				response = cache.get(cache_key)
			except Exception as e:
				response = e
		return HttpResponse(response, 'application/json')

